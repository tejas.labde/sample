import { Injectable } from '@angular/core';
import {HTTPClient} from  '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  constructor(public httpClient:HTTPClient) { }

  getData(){
    return this.httpClient.get('https://saral-dev-api.anuvaad.org/getMarksReport')
  }
}
