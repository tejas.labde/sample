import { Component, OnInit } from '@angular/core';
import { GetDataService } from 'src/service/get-data.service';


@Component({
  selector: 'app-calculated-val',
  templateUrl: './calculated-val.component.html',
  styleUrls: ['./calculated-val.component.css']
})
export class CalculatedValComponent implements OnInit {

  input=[];
  studentScore:number=0;
  studentCount:number=0;

  allCount:number=0;
  partialCount:number=0;
  noneCount:number=0;

  constructor(public getDataService:GetDataService) { }

  ngOnInit(): void {
  }



  async getData(){
    const data=await this.getDataService.getData();
    data.map(student=>{
      this.studentScore=0;
      this.studentCount=0;

      student.marksInfo.map(marksObj => {
        this.studentCount++;
        this.studentScore+=marksObj.obtainedMarks
      })
      if(this.studentScore/this.studentScore==1){
        this.allCount++;
      }
      else if(this.studentScore/this.studentScore<1 && this.studentScore/this.studentScore>0){
        this.partialCount++;
      }
      else{
        this.noneCount++;
      }
    })
  }

}
