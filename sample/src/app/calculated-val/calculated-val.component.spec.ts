import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CalculatedValComponent } from './calculated-val.component';

describe('CalculatedValComponent', () => {
  let component: CalculatedValComponent;
  let fixture: ComponentFixture<CalculatedValComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CalculatedValComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CalculatedValComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
